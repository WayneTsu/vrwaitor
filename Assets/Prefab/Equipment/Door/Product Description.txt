
Such 3D Files are included:

          	- *.max (3ds Max 2010 for separate models)
          	- *.max (3ds Max 2010 for the scene, whose models are introduced as XRef Objects with Proxy Object option)
		- *.max (3ds Max 2012 for the scene, whose models are introduced as XRef Objects with Proxy Object option)
                - *.obj (low poly)
                - *.obj (high poly)
                - *.fbx (low poly)
                - *.fbx (high poly)
		- For all the scenes with XRef Objects, the Proxy Object option for quick saving of the file edition and Viewport drawings is used
		- All paths in the scene are relative to the project folder;
		- 3d model are located in corresponding layers for easy scene management;
		- Named objects\materials\layers for easy navigation

Render Plugin 	                 - Vray2.0

-------------------------------------------------------------------------


Mesh Totals: in scene with XRef Object (Proxy Object enabled)

  Polys: 9 846

Mesh Totals: in scene without XRef Object (all models in)

   Polys: 24 583

folders:
	
	models: 2 files

	maps:   2 files

------------------------------------------------------------------------------------------------------------------

Description: A door

Category: exterior door

Style: classicism

