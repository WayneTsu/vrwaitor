﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HTC.UnityPlugin.Vive;

public class StatusCanvas : MonoBehaviour {

    //  Status Canvas : 代表目前的遊戲狀態。
    //                  裝置在右手的 controller 上。

    #region Vive Equipment

    [SerializeField]
    private GameObject leftHandController, rightHandController;

    #endregion

    #region UI_variables
    [SerializeField]
    private GameObject Canvas;

    [SerializeField]
    private Text countDownText;
    [SerializeField]
    private Image countDownValue;
    [SerializeField]
    private Image backgroundEffect_inner, backgroundEffect_outer;

    [SerializeField]
    private Text statusText;
    #endregion

    #region variables

    /// <summary>
    /// CountDown Ongoing.
    /// </summary>
    bool onGoing;

    /// <summary>
    /// true : alpha up / false ; alpha down
    /// </summary>
    bool brighten;

    #endregion

    // Use this for initialization
    void Start () {

        onGoing = false;
        brighten = false;

        StartCoroutine( backgroundMotion() );
	}
	
	// Update is called once per frame
	void Update () {

        //  Right Hand
        if (ShotManage.gameStart)
        {
            if(ViveInput.GetPress(HandRole.RightHand, ControllerButton.Trigger))
                Canvas.GetComponent<Canvas>().enabled = true;
            else
                Canvas.GetComponent<Canvas>().enabled = false;

            #region countDown
            if (!onGoing)
            {
                //  game onGoing
                
                countDownValue.color = new Color(32.0f / 255.0f, 190.0f / 255.0f, 231.0f / 255.0f);
                countDownText.color = Color.white;
                statusText.text = "OnGoing";
                StartCoroutine(timerCountDown(60.0f));

                onGoing = true;
            }
            #endregion

            #region blink
            if (brighten)
            {
                statusText.color = new Color(statusText.color.r,
                                             statusText.color.g,
                                             statusText.color.b,
                                             statusText.color.a + 0.05f
                                            );
                if (statusText.color.a > 1.0f)
                    brighten = false;
            }
            else
            {
                statusText.color = new Color(statusText.color.r,
                                             statusText.color.g,
                                             statusText.color.b,
                                             statusText.color.a - 0.05f
                                            );
                if (statusText.color.a < 0.3f)
                    brighten = true;
            }
            #endregion
        }
        else
        {
            Canvas.GetComponent<Canvas>().enabled = false;
        }
    }

    /// <summary>
    /// CountDown.
    /// </summary>
    /// <returns></returns>
    private IEnumerator timerCountDown(float countdownSeconds)
    {
        float previous = -1.0f;
        for(float i=0.0f; i< countdownSeconds; i+= Time.deltaTime * 2)
        {
            yield return new WaitForSeconds(Time.deltaTime);

            countDownValue.fillAmount = (countdownSeconds - i) / countdownSeconds;

            float floor = Mathf.Floor(i);

            if(previous != floor)
            {
                int timeValue = (int)countdownSeconds - (int)previous - 1;
                countDownText.text = "00 : " + timeValue.ToString("00");

                previous = floor;
            }
        }
        countDownText.text = "00 : 00";

        onGoing = false;
        ShotManage.gameStart = false;
    }

    private IEnumerator backgroundMotion()
    {
        backgroundEffect_inner.GetComponent<Image>().fillAmount = 1.0f;
        backgroundEffect_outer.GetComponent<Image>().fillAmount = 1.0f;

        float offset = 0.05f;

        while (true)
        {
            yield return new WaitForSeconds(Time.deltaTime);

            backgroundEffect_inner.GetComponent<Image>().fillAmount -= offset;
            backgroundEffect_outer.GetComponent<Image>().fillAmount -= offset;

            if (backgroundEffect_inner.GetComponent<Image>().fillAmount >= 1.0f ||
               backgroundEffect_inner.GetComponent<Image>().fillAmount <= 0.0f)
            {
                offset = -offset;
                backgroundEffect_inner.GetComponent<Image>().fillClockwise = !backgroundEffect_inner.GetComponent<Image>().fillClockwise;
                backgroundEffect_outer.GetComponent<Image>().fillClockwise = !backgroundEffect_outer.GetComponent<Image>().fillClockwise;
            }
                
        }
    }
}
