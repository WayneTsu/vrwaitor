﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitMotion : MonoBehaviour {

    //  兔子 : 每過幾秒會飛天式的跳躍 (向上方) (在盤子上)，
    //  然後在五秒後會在附近落下

    #region variables

    [SerializeField]
    private GameObject rabbitMesh;   //  兔子的 mesh

    private bool onGoing;       //  正在進行中
    private int onPlate;        //  計算在盤子上的時間
    #endregion

    // Use this for initialization
    void Start () {
        onGoing = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (onPlate > 200)
            StartCoroutine(rabbit_motion());
	}

    private IEnumerator rabbit_motion()
    {
        //  跳起來
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.up * 12.0f;
        //  2 秒後
        yield return new WaitForSeconds(1.0f);
        //  消失
        rabbitMesh.SetActive(false);
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

    //  出現倒數的 UI
        //  5 秒後
        yield return new WaitForSeconds(5.0f);

        //  出現在四周的隨機位置
        float _X = Random.Range(-0.5f, 0.5f);
        float _Z = Random.Range(-0.5f, 0.5f);
        gameObject.transform.Translate(new Vector3(_X, 0.0f, _Z));
        rabbitMesh.SetActive(true);

        gameObject.GetComponent<Rigidbody>().useGravity = true;
    //  出現指示的箭頭
        
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        onPlate += 1;
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        onPlate = 0;
    }
}
