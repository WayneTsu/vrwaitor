﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HTC.UnityPlugin.Vive;

public class TorchGun : MonoBehaviour {

    [SerializeField]
    private GameObject flame;       //  火焰噴射器的火焰
    [SerializeField]
    private GameObject flameEject;   //  火焰噴射器的火焰發射點，用於判斷火焰有沒有成功加熱  
    [SerializeField]
    private GameObject trigger;     //  火焰噴射器的 trigger

    [SerializeField]
    private AudioSource hairDryerSound; //  吹風機的聲音
    [SerializeField]
    private AudioSource switchSound;    //  按鍵的聲音
    [SerializeField]
    private AudioSource chargeSound;    //  充電的聲音

    [SerializeField]
    private GameObject batteryBar;
    [SerializeField]
    private Image powerBar;
    [SerializeField]
    private Text powerBarText;

    public float power = 0.0f;
    private bool ejecting;

    private Coroutine hairDryer;

    public void initialHairDryer()
    {
        power = 100.0f;
        ejecting = false;
        batteryBar.SetActive(false);
    }

	// Use this for initialization
	void Start () {
        initialHairDryer();
	}
	
	// Update is called once per frame
	void Update () {

        if (GameManager.instance.currentStage == GameManager.GameStage.Heating &&
            GameManager.instance.currentStep != GameManager.GameStep.PrepareStage)
        {
            //  按下扳機
            if (ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.Trigger))
            {
                switchSound.Play(); //  按鍵聲 : 開始
                FireEjection(true); //  噴火 : 開始
                batteryBar.SetActive(true);

                if(ejecting)
                    flameEject.SetActive(true);
            }
            //  放開扳機
            else if (ViveInput.GetPressUp(HandRole.LeftHand, ControllerButton.Trigger))
            {
                switchSound.Stop(); //  按鍵聲 : 結束
                FireEjection(false);//  噴火 : 結束
                batteryBar.SetActive(false);
                flameEject.SetActive(false);
            }
            
            //  持續的消耗能量
            if(ejecting)
            {
                if (power <= 0.0f)
                {
                    ejecting = false;
                    flame.SetActive(false);
                    flameEject.SetActive(false);
                    hairDryerSound.Stop();
                } else
                {
                    if(GameManager.instance.currentStep == GameManager.GameStep.GameStart)
                    {
                        power -= 0.5f;
                    }                    
                }

                powerBar.fillAmount = power / 100.0f;
                powerBarText.text = Mathf.CeilToInt( power ).ToString() + "%";
            }            
             
        }
        else
        {
            FireEjection(false);

            ejecting = false;
            flame.SetActive(false);
            flameEject.SetActive(false);
            hairDryerSound.Stop();
        }
            
    }

    /// <summary>
    /// 用 flameEject 的位置跟食物的位置來推斷加熱的程度。
    /// </summary>
    /// <returns></returns>
    public float RiseTemperature(Transform foodPosition)
    {
        if (Vector3.Dot(flameEject.transform.up, foodPosition.up) > 0)
            return Vector3.Distance( flameEject.transform.position, foodPosition.position ) / 0.01f;

        return 0.0f;
    }

    /// <summary>
    /// 判斷當前的狀態 (噴火/不噴火) : eject。
    /// </summary>
    /// <param name="status"></param>
    public void FireEjection(bool status)
    {
        if (status)
        {  //  按下 trigger
            //  Trigger Motion.
            trigger.transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), -13.0f, Space.Self);

            //  判斷有沒有能量

            //  當沒有能量的時候，甚麼事都不做。
            if (power <= 0.0f)
            {
                ejecting = false;
                flame.SetActive(false);
            }
            else
            {
                ejecting = true;
                flame.SetActive(true);
            }
        }
        else
        {//  沒有按下 trigger
            //  Trigger Motion.
            trigger.transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), 13.0f, Space.Self);
            flame.SetActive(false);

            ejecting = false;
        }

        if(ejecting && hairDryerSound.isPlaying == false)
        {
            hairDryerSound.Play();
            hairDryer = StartCoroutine(HairyDryerMotion());
        }            
        if (!ejecting && hairDryerSound.isPlaying == true)
            hairDryerSound.Stop();        
    }

    public void ChargePower()
    {
        power += 70.0f;
        chargeSound.Play();
        if (power > 100.0f) power = 100.0f;
    }

    private IEnumerator HairyDryerMotion()
    {
        while (ejecting)
        {
            ViveInput.TriggerHapticPulse(HandRole.LeftHand, 1500);
            yield return new WaitForEndOfFrame();
        }
    }
}
