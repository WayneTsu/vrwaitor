﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Arrow : MonoBehaviour {


    #region Variables
    [SerializeField]
    private Image ArrowImg;         //  箭頭的 UI image

    private bool blinking = false;  //  閃爍效果
    private bool rotate = false;    //  旋轉    
    private bool floating = false;  //  懸浮效果

    [SerializeField]
    private Vector3 floatingDir;    //  懸浮方向

    #endregion

    // Use this for initialization
    void Start () {
        //SetRotate(true);
        SetBlink(true);
        floatingDir = Vector3.up;
        SetFloating(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetArrowColor(Color newColor)
    {
        ArrowImg.color = newColor;
    }

    /// <summary>
    /// Set the floating direction.
    /// </summary>
    /// <param name="dir"></param>
    public void FloatingDirection(Vector3 dir)
    {
        floatingDir = dir;
    }

    /// <summary>
    /// 以 sendMessage 呼叫
    /// </summary>
    /// <param name="target"></param>
    public void PointAt(Transform target)
    {

    }

    public void SetBlink(bool status)
    {
        blinking = status;

        if (blinking)
            StartCoroutine(arrow_blink());
    }

    public void SetRotate(bool status)
    {
        rotate = status;

        if (rotate)
            StartCoroutine(arrow_rotate());
    }

    public void SetFloating(bool status)
    {
        floating = status;

        if (floating)
            StartCoroutine(arrow_floating());
    }

    private IEnumerator arrow_blink()
    {
        float offset = -0.1f;
        while(blinking)
        {
            Color current = new Color( ArrowImg.color.r, ArrowImg.color.g, ArrowImg.color.b, ArrowImg.color.a + offset);
            ArrowImg.color = current;

            yield return new WaitForSeconds(0.1f);

            if (current.a >= 1.0f || current.a <= 0.2f)
                offset = -offset;
        }
    }

    private IEnumerator arrow_rotate()
    {
        while(rotate)
        {
            ArrowImg.gameObject.transform.Rotate(ArrowImg.gameObject.transform.up, 5.0f);
            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator arrow_floating()
    {
        Vector3 directionOffset = floatingDir.normalized / 100.0f;
        Vector3 currentFloatingOffset = Vector3.zero;
        float distance = 0.1f;

        while (floating)
        {
            ArrowImg.gameObject.transform.Translate( directionOffset );
            currentFloatingOffset += directionOffset;

            yield return new WaitForSeconds(Time.deltaTime * 2.0f);

            if (currentFloatingOffset.magnitude > distance)
                directionOffset = -directionOffset;
        }
    }
}
