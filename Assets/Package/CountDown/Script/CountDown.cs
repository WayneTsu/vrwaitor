﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {

    #region variables

    [SerializeField]
    private GameObject countDownPrefab;
    //  倒數計時的 UI text
    private Text countDownText;

    //  倒數結束
    private bool countDownTaskOver;  

    [SerializeField]
    //  最大字型、最小字型
    private int maxFontSize = 256, minFontSize = 16;

    #endregion

    // Use this for initialization
    void Start () {
        countDownText = countDownPrefab.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    /// <summary>
    /// 用 sendmessage 來呼叫。
    /// </summary>
    /// <param name="seconds"></param>
    public void StartCountingDown(float seconds)
    {
        countDownPrefab.SetActive(true);
        countDownTaskOver = false;
        StartCoroutine( CountingDown(seconds) );
    }

    private IEnumerator CountingDown(float seconds)
    {
        float prev = -1.0f;

        if (seconds == 3.0f)
            gameObject.GetComponent<AudioSource>().Play();

        for(float i = 0; i<seconds; i+=Time.deltaTime* 2 )
        {
            yield return new WaitForSeconds(Time.deltaTime);

            float ceiling = Mathf.Ceil(i);
            float floor = Mathf.Floor(i);

            if(prev != floor)
            {
                countDownText.text = (seconds - floor).ToString();
                countDownText.fontSize = maxFontSize;

                prev = floor;
            } else
            {
                float ratio = ( i - floor ) / 1.0f;
                countDownText.fontSize = (int)(minFontSize * ratio) + (int)(maxFontSize * (1.0f - ratio));
            }            
        }

        countDownText.text = "0";
        countDownText.fontSize = minFontSize;

        for (float i = 0; i<1.7f; i+= Time.deltaTime * 2)
        {
            yield return new WaitForSeconds(Time.deltaTime);

            countDownText.fontSize = (int)(maxFontSize * i) + (int)(minFontSize * (1.0f - i));
        }        

        yield return new WaitForSeconds(0.5f);

        countDownPrefab.SetActive(false);
        countDownTaskOver = true;

        ShotManage.gameStart = true;
    }

    public bool CountingDownTaskOver()
    {
        return countDownTaskOver;
    }
}
