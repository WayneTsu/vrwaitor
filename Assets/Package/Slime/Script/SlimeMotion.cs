﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeMotion : MonoBehaviour {

    //  史萊姆 : 落進盤子三秒後，會往盤子外面爬

    #region variables

    //  啟動動作的開關
    private bool motionTrigger;
    private bool onGoing;

    #endregion

    // Use this for initialization
    void Start () {
        motionTrigger = false;
        onGoing = false;
    }
	
	// Update is called once per frame
	void Update () {
		if(motionTrigger)   {
            if(!onGoing)            
                StartCoroutine( slime_motion() );            
            else
                gameObject.transform.Translate(transform.forward * 0.005f);
            
        }
    }

    private IEnumerator slime_motion()
    {
        motionTrigger = false;

        yield return new WaitForSeconds(3.0f);

        onGoing = true;
        motionTrigger = true;
        gameObject.GetComponent<Animator>().SetBool("moving", true);
    }

    void OnCollisionEnter(Collision other)
    {
        motionTrigger = true;
        onGoing = false;

        float angle = Random.Range(30.0f, 90.0f);
        gameObject.transform.Rotate(transform.up, angle);
        //  set the colliding object as parent.
        gameObject.transform.SetParent(other.gameObject.transform);
        gameObject.GetComponent<Animator>().SetBool("moving", false);
    }

    void OnCollisionExit(Collision other)
    {
        //  手把震動
    }
}
