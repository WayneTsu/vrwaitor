﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMotion : MonoBehaviour {

    //  魚 : 每過幾秒，會隨機跳動

    #region variables
    public bool onGoing;

    #endregion

    [SerializeField]
    private AudioSource hitOnGround;

    [SerializeField]
    private AudioSource hitPlate;

    // Use this for initialization
    void Start () {
        onGoing = true;

        StartCoroutine(fish_motion());
	}
	
	// Update is called once per frame
	void Update () {
		          
	}

    private IEnumerator fish_motion()
    {
        while(onGoing)
        {
            float seconds = Random.Range(3.0f, 5.0f);

            yield return new WaitForSeconds(seconds);

            float power = Random.Range(3.0f, 7.5f);
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.up * power;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        //  碰到地板
        if (other.gameObject.CompareTag("Floor"))
        {
            GameManager.instance.eatenFish += 1;
            //  產生新的魚
            StartCoroutine(GameManager.instance.respondToMissingFood(1.0f));

            hitOnGround.Play();

            onGoing = false;
            Destroy(gameObject, 1.5f);            
        }
        else if(other.gameObject.CompareTag("RightPlate"))
        {
            StartCoroutine( GameManager.instance.Pulse(0.3f, false) );

            hitPlate.Play();
        }
    }
}
