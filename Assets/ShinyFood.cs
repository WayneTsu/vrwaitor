﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(ParticleSystem))]
public class ShinyFood : MonoBehaviour {

    private ParticleSystem ps;
    public AudioSource audio;
    private bool start = false;
    private float time_stamp;
	// Use this for initialization
	void Start () {
        ps = GetComponent<ParticleSystem>();
        
	}
	
	// Update is called once per frame
	void Update () {
        if (start)
        {
            if (Time.time - time_stamp > 3.0f)
            {
                ps.Stop();
                start = false;
                //audio.Stop();
            }
            else
            {
                var sh = ps.shape;
                sh.enabled = true;
                if (sh.angle < 90.0f)
                    sh.angle += 20.0f * Time.deltaTime;
                else
                    sh.angle = 90.0f;
                //ps.startSpeed = 8;
                //ps.startLifetime = 3;
            }
        }
        
	}
    public void Play()
    {
        time_stamp = Time.time;
        start = true;
        audio.Play();
        ps.Play();
    }
}
