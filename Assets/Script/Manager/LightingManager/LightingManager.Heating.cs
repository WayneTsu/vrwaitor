﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class LightingManager {

    [Header("Lighting_Heating")]
    #region 光源配置
    [SerializeField]
    private GameObject central_pointLight;
    [SerializeField]
    private GameObject highlightBattery;
    #endregion
    private Coroutine Heating_Lighting;

    public void Heating_DynamicLighting(bool start)
    {
        if(start)
        {
            Heating_SetLightsActive(false);
        }
        else
        {
            Heating_SetLightsActive(false);
        }
    }

    private void Heating_SetLightsActive(bool open, int ind = -1)
    {
        if(ind == -1)
        {
            central_pointLight.SetActive(open);
            highlightBattery.SetActive(open);
        }
        else
        {
            if (ind == 0)
                central_pointLight.SetActive(open);
            else if (ind == 1)
                highlightBattery.SetActive(open);
        }
    }

}
