﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class LightingManager
{
    [Header("Lighting_Fish")]
    #region 光源配置
    //  0, 1, 2 : 左方
    //  3, 4, 5 : 右方
    [SerializeField]
    private GameObject[] Fish_Lights;
    #endregion
    private Coroutine Fish_Lighting;

    public void Fish_DynamicLighting(bool start)
    {
        if (start)
        {
            Fish_SetLightsActive(false);
            Fish_Lighting = StartCoroutine(FishLightMotion());
        }            
        else
        {
            StopCoroutine(Fish_Lighting);
            Fish_SetLightsActive(false);
        }            
    }

    private void Fish_SetLightsActive(bool open, int ind = -1) {
        if (ind == -1)
        {
            for (int i = 0; i < Fish_Lights.Length; i++)
                Fish_Lights[i].SetActive(open);
        }
        else
            Fish_Lights[ind].SetActive(open);        
    }

    private IEnumerator FishLightMotion()
    {
        Color[] fiveColors = new Color[5];
        fiveColors[0] = Color.red;
        fiveColors[1] = Color.blue;
        fiveColors[2] = Color.green;
        fiveColors[3] = Color.cyan;
        fiveColors[4] = Color.magenta;

        for (int i = 0; i < 5; i++)
        {
            for(int j=0; j<Fish_Lights.Length; j++)
                ChangeLightsColor(fiveColors[i], Fish_Lights[j].GetComponent<Light>(), 0.5f );

            Fish_SetLightsActive(false);
            yield return new WaitForSeconds(0.55f);
            Fish_SetLightsActive(true, 0);
            Fish_SetLightsActive(true, 1);
            Fish_SetLightsActive(true, 2);
            yield return new WaitForSeconds(0.55f);
            Fish_SetLightsActive(false);
            yield return new WaitForSeconds(0.55f);
            Fish_SetLightsActive(true, 3);
            Fish_SetLightsActive(true, 4);
            Fish_SetLightsActive(true, 5);
            yield return new WaitForSeconds(0.55f);
            Fish_SetLightsActive(false);
            yield return new WaitForSeconds(0.55f);
            Fish_SetLightsActive(true, 1);
            Fish_SetLightsActive(true, 3);
            Fish_SetLightsActive(true, 5);
            yield return new WaitForSeconds(0.55f);
            Fish_SetLightsActive(false);
            yield return new WaitForSeconds(0.55f);
            Fish_SetLightsActive(true, 0);
            Fish_SetLightsActive(true, 2);
            Fish_SetLightsActive(true, 4);
            yield return new WaitForSeconds(0.55f);
        }

        Fish_SetLightsActive(false);
        yield return new WaitForSeconds(1.0f);
        Fish_SetLightsActive(true);

        for (int j = 0; j < Fish_Lights.Length; j++)
            ChangeLightsColor(Color.yellow, Fish_Lights[j].GetComponent<Light>(), 0.5f);

        #region 左方的光源
        StartCoroutine(RotateLights(0, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f));
        StartCoroutine(RotateLights(1, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f));
        StartCoroutine(RotateLights(2, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f));
        #endregion

        #region 右方的光源
        StartCoroutine(RotateLights(3, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f));
        StartCoroutine(RotateLights(4, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f));
        StartCoroutine(RotateLights(5, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f));
        #endregion

        yield return new WaitForSeconds(5.01f);

        #region 左方的光源
        StartCoroutine(RotateLights(0, new Vector3(0.0f, 1.0f, 0.0f), 180.0f, 10.0f));
        StartCoroutine(RotateLights(1, new Vector3(0.0f, 1.0f, 0.0f), 180.0f, 10.0f));
        StartCoroutine(RotateLights(2, new Vector3(0.0f, 1.0f, 0.0f), 180.0f, 10.0f));
        #endregion

        #region 右方的光源
        StartCoroutine(RotateLights(3, new Vector3(0.0f, 1.0f, 0.0f), 180.0f, 10.0f));
        StartCoroutine(RotateLights(4, new Vector3(0.0f, 1.0f, 0.0f), 180.0f, 10.0f));
        StartCoroutine(RotateLights(5, new Vector3(0.0f, 1.0f, 0.0f), 180.0f, 10.0f));
        #endregion

        yield return new WaitForSeconds(10.01f);

        #region 左方的光源
        StartCoroutine(RotateLights(0, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f));
        StartCoroutine(RotateLights(1, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f));
        StartCoroutine(RotateLights(2, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f));
        #endregion

        #region 右方的光源
        StartCoroutine(RotateLights(3, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f));
        StartCoroutine(RotateLights(4, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f));
        StartCoroutine(RotateLights(5, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f));
        #endregion

        yield return new WaitForSeconds(5.01f);

        Fish_SetLightsActive(false, 0);
        Fish_SetLightsActive(false, 2);
        Fish_SetLightsActive(false, 4);
        
        yield return new WaitForSeconds(1.26f);
    }
}
