﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class LightingManager {
    [Header("Lighting_Fruits")]

    #region 四盞光源 (spotlights) : 呈一個方形排列
    [SerializeField]
    private GameObject[] cornerLights;
    //  4 : LU, LD, RD, RU
    #endregion

    private Coroutine Fruits_Lighting;

    void Fruits_DynamicLighting(bool start)
    {
        if (start)
        {
            Fruits_SetLightsActive(false);
            Fruits_Lighting = StartCoroutine(FruitsLightMotion());
        }            
        else
        {
            StopCoroutine(Fruits_Lighting);
            Fruits_SetLightsActive(false);
        }            
    }

    private void Fruits_SetLightsActive(bool open, int ind = -1)
    {
        if (ind == -1)
        {
            for (int i = 0; i < 4; i++)
                cornerLights[i].SetActive(open);
        }
        else
        {
            cornerLights[ind].SetActive(open);
        }            
    }

    private IEnumerator FruitsLightMotion()
    {
        yield return new WaitForSeconds(1.5f);
        Fruits_SetLightsActive(true, 0);
        yield return new WaitForSeconds(0.3f);
        Fruits_SetLightsActive(true, 1);
        yield return new WaitForSeconds(0.3f);
        Fruits_SetLightsActive(true, 2);
        yield return new WaitForSeconds(0.3f);
        Fruits_SetLightsActive(true, 3);

        for(int k=0; k<5; k++ )
        {
            Debug.Log("k = " + k);
            //  LU, LD, RU, RD
            RotateLights(0, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f);
            RotateLights(1, new Vector3(0.0f, 0.0f, 1.0f), 60.0f, 5.0f);
            RotateLights(2, new Vector3(0.0f, 0.0f, 1.0f), -60.0f, 5.0f);
            RotateLights(3, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f);

            yield return new WaitForSeconds(5.01f);

            RotateLights(0, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f);
            RotateLights(1, new Vector3(0.0f, 0.0f, 1.0f), -60.0f, 5.0f);
            RotateLights(2, new Vector3(0.0f, 0.0f, 1.0f), 60.0f, 5.0f);
            RotateLights(3, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f);

            yield return new WaitForSeconds(5.01f);

            RotateLights(0, new Vector3(0.0f, 0.0f, 1.0f), 60.0f, 5.0f);
            RotateLights(1, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f);
            RotateLights(2, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f);
            RotateLights(3, new Vector3(0.0f, 0.0f, 1.0f), -60.0f, 5.0f);

            yield return new WaitForSeconds(5.01f);

            RotateLights(0, new Vector3(0.0f, 0.0f, 1.0f), -60.0f, 5.0f);
            RotateLights(1, new Vector3(1.0f, 0.0f, 0.0f), -60.0f, 5.0f);
            RotateLights(2, new Vector3(1.0f, 0.0f, 0.0f), 60.0f, 5.0f);
            RotateLights(3, new Vector3(0.0f, 0.0f, 1.0f), 60.0f, 5.0f);

            yield return new WaitForSeconds(5.01f);

            Color[] light_colors = new Color[4];
            light_colors[0] = cornerLights[0].GetComponent<Light>().color;
            light_colors[1] = cornerLights[1].GetComponent<Light>().color;
            light_colors[2] = cornerLights[2].GetComponent<Light>().color;
            light_colors[3] = cornerLights[3].GetComponent<Light>().color;

            //  漸進式的改顏色。
            StartCoroutine(ChangeLightsColor(light_colors[3], cornerLights[0].GetComponent<Light>(), 2.0f));
            StartCoroutine(ChangeLightsColor(light_colors[0], cornerLights[1].GetComponent<Light>(), 2.0f));
            StartCoroutine(ChangeLightsColor(light_colors[1], cornerLights[2].GetComponent<Light>(), 2.0f));
            StartCoroutine(ChangeLightsColor(light_colors[2], cornerLights[3].GetComponent<Light>(), 2.0f));
        }
    }
}
