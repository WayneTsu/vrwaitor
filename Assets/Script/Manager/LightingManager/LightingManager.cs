﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class LightingManager : MonoBehaviour {
    [Header("The Lighting Configurations")]

    [SerializeField]    //  Scoring 階段的光源配置
    private GameObject ScoringLighting;
    [SerializeField]    //  遊戲階段的光源配置
    private GameObject StageLighting;
    [SerializeField]    //  準備階段的光源配置
    private GameObject MainLighting;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //  開啟某些狀態的光源配置。

    #region 啟用光源配置
    public void ApplyMainSetting()
    {
        ScoringLighting.SetActive(false);
        StageLighting.SetActive(false);
        MainLighting.SetActive(true);
    }

    public void ApplyStageSetting()
    {
        ScoringLighting.SetActive(false);
        StageLighting.SetActive(true);
        MainLighting.SetActive(false);
    }

    public void ApplyScoringSetting()
    {
        ScoringLighting.SetActive(true);
        StageLighting.SetActive(false);
        MainLighting.SetActive(false);
    }

    #endregion

    public void UseStageLighting(bool open)
    {
        switch(GameManager.instance.currentStage)
        {
            case GameManager.GameStage.Fish:
                Fish_DynamicLighting(open);
                break;
            case GameManager.GameStage.Fruits:
                Fruits_DynamicLighting(open);
                break;
            case GameManager.GameStage.Heating:

                break;
        }
    }

    private IEnumerator RotateLights(int ind, Vector3 axis, float angle, float seconds )
    {
        for (float i = 0.0f; i < seconds; i += Time.deltaTime )
        {
            yield return new WaitForSeconds(Time.deltaTime);

            switch(GameManager.instance.currentStage)
            {
                case GameManager.GameStage.Fish:                    
                    Fish_Lights[ind].transform.Rotate(axis, angle / seconds * Time.deltaTime, Space.World);
                    break;

                case GameManager.GameStage.Fruits:
                    cornerLights[ind].transform.Rotate(axis, angle / seconds * Time.deltaTime, Space.World);
                    break;

                case GameManager.GameStage.Heating:
                    if(ind == 0)
                        central_pointLight.transform.Rotate(axis, angle / seconds * Time.deltaTime, Space.World);
                    else
                        highlightBattery.transform.Rotate(axis, angle / seconds * Time.deltaTime, Space.World);

                    break;
            }

        }
    }

    private IEnumerator ChangeLightsColor(Color newColor, Light target, float seconds)
    {
        Color origin = target.color;

        for (float i = 0.0f; i < seconds; i += Time.deltaTime)
        {
            yield return new WaitForSeconds(Time.deltaTime);

            target.color = origin * (seconds - i) / seconds + newColor * i / seconds;
        }
    }
}
