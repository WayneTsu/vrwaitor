﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    [SerializeField]
    private AudioSource beforeGameStart__Background;                    //  遊戲開始前的背景聲音
    [SerializeField]
    private AudioSource Gaming__Fish, Gaming__Fruits, Gaming__Heating;  //  每個遊戲的主題曲
    [SerializeField]
    private AudioSource scoring__Background;                            //  計分背景

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Fish_ThemeSong(bool play, float delay = 0.0f)
    {
        if (play)
            Gaming__Fish.PlayDelayed(delay);
        else
            Gaming__Fish.Stop();
    }

    public void Fruits_ThemeSong(bool play, float delay = 0.0f)
    {
        if (play)
            Gaming__Fruits.PlayDelayed(delay);
        else
            Gaming__Fruits.Stop();
    }

    public void Heating_ThemeSong(bool play, float delay = 0.0f)
    {
        if (play)
            Gaming__Heating.PlayDelayed(delay);
        else
            Gaming__Heating.Stop();
    }

    public void beforeGameStart_ThemeSong(bool play, float delay = 0.0f)
    {
        if (play)
            beforeGameStart__Background.PlayDelayed(delay);
        else
            beforeGameStart__Background.Stop();
    }
    
    public void scoring_ThemeSong(bool play, float delay = 0.0f)
    {
        if (play)
            scoring__Background.PlayDelayed(delay);
        else
            scoring__Background.Stop();
    }
}
