﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using HTC.UnityPlugin.Vive;

public class GameManager : MonoBehaviour {

    //  遊戲進程控制 (掌管計時、分數、UI出現

    //  物理類 (雙手都有盤子，兩盤都有食物): Fruits, Fish
    //  互動類 (只有單手的盤子，另一手拿其他工具): Chicken (拿噴槍)
    //  特殊物理類 (雙手都有盤子，但食物只會在其中一盤): Rabbit, Slime

    #region 全域變數
    public static GameManager instance; //  singleton

    #endregion

    #region 區域變數

    #region 自定義型態 : GameStage
    public enum GameStage { Rabbit, Slime, Heating, Fruits, Fish  }
    #endregion

    [SerializeField]
    //  關卡流程控制 : 預備進關階段、預備手把階段、預備食物器材階段、正式進關
    public enum GameStep { PrepareStage, PreparePlate, PrepareFoodTool, GameStart, Scoring  }

    [HideInInspector]
    public GameStep currentStep;

    public GameStage currentStage;

    private StageManager stageManager;              //  遊戲內容控制
    private PlateManager plateManager;              //  盤子的碰撞控制

    [SerializeField]
    private MusicManager musicManager;              //  音樂控制

    [SerializeField]
    private LightingManager lightingManager;        //  光源控制

    [SerializeField]
    private GameObject viveInputReady;              //  ViveInput Ready

    [SerializeField]
    private Text UIHint;                            //  UI，提示訊息
    [SerializeField]
    private GameObject UIArrow_Left, UIArrow_Right; //  UI，指示手把擺放位置 (左，右
    [SerializeField]
    private GameObject pearIcon, fireIcon, fishIcon;

    private AudioSource headingIntoNewStep;         //  進入新階段的提示音
    [SerializeField]
    public GameObject countDown;                   //  3,2,1 倒數計時的物件

    private bool scoringPhaseDone;                  //  已完成 Scoring 

    private bool buffering;                         //  是否正在進行進關前後的緩衝

    [HideInInspector]
    public bool countDownIsOver;

    #endregion

    #region 初始化變數
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);

        stageManager = GetComponent<StageManager>();
        plateManager = GetComponent<PlateManager>();

        currentStep = GameStep.PrepareStage;
        currentStage = GameStage.Fruits;

        headingIntoNewStep = GetComponent<AudioSource>();

        buffering = false;
    }

    // Use this for initialization
    void Start () {
        InitialStep();
	}
    #endregion

    #region 常數
    private string PrepareStage = "請點選按鈕選擇關卡";
    private string PreparePlate = "請將手把放在箭頭下方";
    private string PrepareFoodTool = "同時按下雙手扳機開始遊戲";
    private string Scoring = "同時按下雙手扳機重新開始";
    #endregion

    // Update is called once per frame
    void Update () {        
        CheckIfConditionSatisfied();
    }

    /// <summary>
    /// 進到下一關。
    /// </summary>
    public void HeadingToNewStage( GameStage stageName )
    {
        currentStage = stageName;
    }

    /// <summary>
    /// 檢查進入下一關的條件齊備了沒。
    /// PrepareStage : 按下 Trigger
    /// PreparePlate : 將 controller 至於指定位置
    /// PrepareFood & Tool : 按下 雙手 Trigger
    /// GameStart : 計時結束
    /// </summary>
    private void CheckIfConditionSatisfied()
    {
        bool isSatisfied = false;

        if (buffering)
            return;

        switch (currentStep)
        {
            case GameStep.PrepareStage:
                //  任何一個按鈕被按下。
                isSatisfied = (pearIcon.GetComponent<IconCollider>().isReady || 
                        fireIcon.GetComponent<IconCollider>().isReady || 
                        fishIcon.GetComponent<IconCollider>().isReady ) ;
                break;
            case GameStep.PreparePlate:
                isSatisfied = viveInputReady.GetComponent<ViveInputGetReady>().IsReady() ;
                break;
            case GameStep.PrepareFoodTool:
                isSatisfied = (ViveInput.GetPress(HandRole.LeftHand, ControllerButton.Trigger) &&
                               ViveInput.GetPress(HandRole.RightHand, ControllerButton.Trigger)) ;
                break;
            case GameStep.GameStart:
                if(countDown.GetComponent<CountDown>().CountingDownTaskOver() == true)
                {
                    countDownIsOver = true;
                    isSatisfied = !ShotManage.gameStart;                    
                }
                    
                break;
            case GameStep.Scoring:
                isSatisfied = (scoringPhaseDone && (ViveInput.GetPress(HandRole.LeftHand, ControllerButton.Trigger) &&
                                                    ViveInput.GetPress(HandRole.RightHand, ControllerButton.Trigger)));
                if (scoringPhaseDone)
                    UIHint.text = Scoring;                 
                break;
        }

        
        //  進入下一個 step (呼叫 初始化跟收尾
        if (isSatisfied)
        {
            ExitStep();
            headingIntoNewStep.Play();
            switch (currentStep)
            {
                case GameStep.PrepareStage:
                    currentStep = GameStep.PreparePlate;
                    break;
                case GameStep.PreparePlate:
                    currentStep = GameStep.PrepareFoodTool;
                    break;
                case GameStep.PrepareFoodTool:
                    currentStep = GameStep.GameStart;
                    break;
                case GameStep.GameStart:
                    currentStep = GameStep.Scoring;
                    break;
                case GameStep.Scoring:
                    currentStep = GameStep.PrepareStage;
                    break;
            }
            InitialStep();
        }
        else
        {
            switch (currentStep)
            {
                case GameStep.GameStart:


                    break;
            }
        }
    }

    /// <summary>
    /// 離開一個舊的 step 的收尾工作。(只會做一次)
    /// 
    /// </summary>
    private void ExitStep()
    {
        switch (currentStep)
        {
            case GameStep.PrepareStage:
                pearIcon.SetActive(false);
                fireIcon.SetActive(false);
                fishIcon.SetActive(false);
                
                break;
            case GameStep.PreparePlate:
                UIArrow_Left.SetActive(false);
                UIArrow_Right.SetActive(false);

                break;
            case GameStep.PrepareFoodTool:
                break;
            case GameStep.GameStart:
                PlayStageThemeSong(false);

                UIHint.gameObject.SetActive(true);
                ShotManage.gameStart = false;

                stageManager.HideEveryTools();  //  hide every tools.
                lightingManager.UseStageLighting(false);
                switch (currentStage)
                {
                    case GameStage.Fish:
                        //eatenFish = 0;
                        break;
                    case GameStage.Fruits:
                        //catchFruits = 0;
                        break;
                    case GameStage.Heating:
                        //roastedChicken = 0;
                        break;
                }

                break;
            case GameStep.Scoring:
                Scoring_Equipment.SetActive(false);
                Food_Cover.SetActive(false);
                Food_Shinny_particle.SetActive(false);
                pearIcon.GetComponent<IconCollider>().resetButtonStatus();
                fireIcon.GetComponent<IconCollider>().resetButtonStatus();
                fishIcon.GetComponent<IconCollider>().resetButtonStatus();

                StartCoroutine(bufferingTime(3.0f));
                break;
        }
    }

    /// <summary>
    /// 進入一個新的 step 的初始化工作。 (只會做一次)
    /// UI 部份 : 出現提示字眼。
    /// 
    /// </summary>
    
    private void InitialStep()
    {
        switch (currentStep)
        {
            case GameStep.PrepareStage:
                lightingManager.ApplyMainSetting();
                hidingAllScoringUIMenu();                

                musicManager.scoring_ThemeSong(false);
                musicManager.beforeGameStart_ThemeSong(true, 0.5f);

                UIHint.text = PrepareStage;

                pearIcon.SetActive(true);
                fireIcon.SetActive(true);
                fishIcon.SetActive(true);

                pearIcon.GetComponent<IconCollider>().isReady = false;
                fireIcon.GetComponent<IconCollider>().isReady = false;
                fishIcon.GetComponent<IconCollider>().isReady = false;

                break;
            case GameStep.PreparePlate:
                UIHint.text = PreparePlate;
                UIArrow_Left.SetActive(true);
                UIArrow_Right.SetActive(true);

                stageManager.toolPrepared(currentStage);
                break;
            case GameStep.PrepareFoodTool:
                UIHint.text = PrepareFoodTool;
                stageManager.foodPrepared(currentStage);
                stageManager.shotPrepared(currentStage);

                break;
            case GameStep.GameStart:
                lightingManager.ApplyStageSetting();

                musicManager.beforeGameStart_ThemeSong(false);
                PlayStageThemeSong(true, 4.5f);

                UIHint.text = "";
                UIHint.gameObject.SetActive(false);
                countDown.GetComponent<CountDown>().StartCountingDown(3.0f);

                InitialVariable();
                countDownIsOver = false;
                
                ShotManage.ready = true;
                lightingManager.UseStageLighting(true);
                break;
            case GameStep.Scoring:
                lightingManager.ApplyScoringSetting();

                PlayStageThemeSong(false);
                musicManager.scoring_ThemeSong(true);

                Scoring_Equipment.SetActive(true);
                Food_Cover.SetActive(true);
                Food_Shinny_particle.SetActive(true);

                //  ISSUE 3 : Food_Cover 沒有回到原位
                PlacingTheFoodCover();

                scoringPhaseDone = false;

                StartCoroutine(Scoring_FirstPhase());
                
                break;                
        }
    }

    private void PlayStageThemeSong(bool play, float delay = 0.0f)
    {
        switch(currentStage) {
            case GameStage.Fish:
                musicManager.Fish_ThemeSong(play, delay);
                break;
            case GameStage.Fruits:
                musicManager.Fruits_ThemeSong(play, delay);
                break;
            case GameStage.Heating:
                musicManager.Heating_ThemeSong(play, delay);
                break;
        }
    }

    /// <summary>
    /// 當盤子上的東西被拿走，系統的回應。
    /// </summary>
    public IEnumerator respondToMissingFood(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        switch(currentStage)
        {
            case GameStage.Fish:
                stageManager.createNewFishPrefab();
                break;
            case GameStage.Fruits:
                break;
            case GameStage.Heating:
                break;
        }
    }

    /// <summary>
    /// 進關卡的緩衝時間
    /// </summary>
    /// <param name="seconds"></param>
    /// <returns></returns>
    private IEnumerator bufferingTime(float seconds)
    {
        buffering = true;

        yield return new WaitForSeconds(seconds);

        pearIcon.GetComponent<IconCollider>().activated = true;
        fireIcon.GetComponent<IconCollider>().activated = true;
        fishIcon.GetComponent<IconCollider>().activated = true;

        buffering = false;
    }

    public void CatchingFruits(Transform _transform)
    {
        stageManager.GetComponent<StageManager>().CatchingFruits(_transform);
    }

    #region 評分階段
        #region 評分階段 : UI 常數
        [Header("UI Menu")]
            #region Menu
            [SerializeField]
            private GameObject WholeMenu;
            [SerializeField]
            private GameObject FishMenu;        //  魚的 Scoring Menu
            [SerializeField]
            private GameObject FruitsMenu;      //  水果的 Scoring Menu
            [SerializeField]
            private GameObject HeatingMenu;     //  加熱的 Scoring Menu
            #endregion
            
            [Header("UI Menu Values")]
            #region Value
            [SerializeField]    //  消耗的魚的數量
            private GameObject FishMenuValue;
            [SerializeField]    //  接到的水果數量
            private GameObject FruitsMenuValue;
            [SerializeField]    //  最終的加熱溫度
            private GameObject HeatingMenuValue;
            #endregion

            [SerializeField]
            private AudioSource BeepSound;
            [SerializeField]
            private AudioSource FleeSound;
            [SerializeField]
            private AudioSource BoilSound;
        #endregion

        #region 評分階段 : 常數
            [HideInInspector]
            public int eatenFish;          //  Fish
            [HideInInspector]
            public int catchFruits;        //  Fruits
            [HideInInspector]
            public float roastedChicken;     //  Heating
        #endregion

        #region 評分階段 : gameObjects
        [SerializeField]
        private GameObject Scoring_Equipment;
        [SerializeField]
        private GameObject Food_Cover;
        [SerializeField]
        private Transform foodCoverPosition;
        [SerializeField]
        private GameObject Food_Shinny_particle;
        [SerializeField]
        private GameObject[] End_Plate_Fruitng;
        [SerializeField]
        private GameObject End_Plate_Fishing;
        [SerializeField]
        private GameObject End_Plate_Heating;
    #endregion

    #region  評分階段 : 函式
    /// <summary>
    /// 隱藏所有 UI Menu.
    /// </summary>
    private void hidingAllScoringUIMenu()
            {
                hidingScoringUIMenu(GameStage.Fish);
                hidingScoringUIMenu(GameStage.Fruits);
                hidingScoringUIMenu(GameStage.Heating);                
            }
        
            /// <summary>
            /// 隱藏特定的UI Menu.
            /// </summary>
            /// <param name="stage"></param>
            private void hidingScoringUIMenu(GameStage stage)
            {
                switch (stage)
                {
                    case GameStage.Fish:
                        FishMenu.SetActive(false);
                        break;
                    case GameStage.Fruits:
                        FruitsMenu.SetActive(false);
                        break;
                    case GameStage.Heating:
                        HeatingMenu.SetActive(false);
                        break;
                }
            }
            
            /// <summary>
            /// 初始化 Variable (Game Start 時呼叫)
            /// </summary>
            /// <param name="stage"></param>
            private void InitialVariable()
            {
                switch (currentStage)
                {
                    case GameStage.Fish:
                        eatenFish = 0;
                        break;
                    case GameStage.Fruits:
                        catchFruits = 0;
                        break;
                    case GameStage.Heating:
                        roastedChicken = 0;
                        break;
                }
            }

            private void SetEatenFish(int value)
            {
                FishMenuValue.GetComponent<Text>().text = eatenFish.ToString() + " 條";
            }

            private void SetCatchFruits(int value)
            {
                FruitsMenuValue.GetComponent<Text>().text = catchFruits.ToString() + " 個";
            }

            private void SetRoastedChicken(int value)
            {
                HeatingMenuValue.GetComponent<Text>().text = roastedChicken.ToString() + " 度C";
            }

            private IEnumerator ScoringEffect(float from, float to, float seconds)
            {
                BeepSound.Play();
                for (float time = 0.0f; time < seconds; time += Time.deltaTime * 2.0f)
                {
                    yield return new WaitForSeconds(Time.deltaTime);
                    
                    switch( currentStage )
                    {
                        case GameStage.Fish:
                            SetEatenFish((int)from);
                            break;
                        case GameStage.Fruits:
                            SetCatchFruits((int)from);
                        break;
                        case GameStage.Heating:
                            SetRoastedChicken((int)from);
                        break;
                    }

                    from += (to - from) / seconds * Time.deltaTime;       

                }
                BeepSound.Stop();
            }
            
            private void SetupEndPlate()
            {
                switch(currentStage)
                {
                    case GameStage.Fish:
                        {
                            for(int i=0;i<eatenFish;i++)
                            {
                                GameObject gameObject = Instantiate(End_Plate_Fishing, foodCoverPosition.position + new Vector3(0.0f, 1.5f, 0.0f), Quaternion.identity ) as GameObject;
                                gameObject.transform.Rotate( gameObject.transform.forward, 90.0f );
                            }
                        }
                        break;
                    case GameStage.Fruits:
                        for(int i=0;i<catchFruits;i++)
                        {
                            int ind = Random.Range(0, End_Plate_Fruitng.Length);
                            Instantiate(End_Plate_Fruitng[ind], foodCoverPosition.position + new Vector3(0.0f, 1.5f, 0.0f), Quaternion.identity);
                        }
                        break;
                    case GameStage.Heating:
                        Instantiate(End_Plate_Heating, foodCoverPosition.position + new Vector3(0.0f, 0.64f, 0.44f), Quaternion.identity);
                        break;
                }
        //End_Plate_Fruitng.transform.position = foodCoverPosition.position;
        //End_Plate_Fishing.transform.position = foodCoverPosition.position;
        //End_Plate_Heating.transform.position = foodCoverPosition.position;
        //switch (currentStage)
        //        {
        //            case GameStage.Fish:
        //        End_Plate_Fruitng.SetActive(false);
        //        End_Plate_Fishing.SetActive(true);
        //        End_Plate_Heating.SetActive(false);
        //        break;
        //            case GameStage.Fruits:
        //        End_Plate_Fruitng.SetActive(true);
        //        End_Plate_Fishing.SetActive(false);
        //        End_Plate_Heating.SetActive(false);
        //        break;
        //            case GameStage.Heating:
        //        End_Plate_Fruitng.SetActive(false);
        //        End_Plate_Fishing.SetActive(false);
        //        End_Plate_Heating.SetActive(true);
        //        break;
                //}
            }           
            private void OpenUpTheScoringMenu()
            {
                WholeMenu.SetActive(true);
                switch(currentStage)
                {
                    case GameStage.Fish:
                        FishMenu.SetActive(true);
                        FruitsMenu.SetActive(false);
                        HeatingMenu.SetActive(false);
                        break;
                    case GameStage.Fruits:
                        FishMenu.SetActive(false);
                        FruitsMenu.SetActive(true);
                        HeatingMenu.SetActive(false);
                        break;
                    case GameStage.Heating:
                        FishMenu.SetActive(false);
                        FruitsMenu.SetActive(false);
                        HeatingMenu.SetActive(true);
                        break;
                }
            }

            private IEnumerator Scoring_FirstPhase()
            {
                Food_Cover.GetComponent<FoodCover>().PrepareFoodPosition();
                Food_Shinny_particle.transform.position = foodCoverPosition.position;
                
                BoilSound.Play();
                yield return new WaitForSeconds(5.0f);

                Food_Cover.GetComponent<FoodCover>().FoodCoverMotion();
                Food_Shinny_particle.GetComponent<ShinyFood>().Play();
                SetupEndPlate();

                yield return new WaitForSeconds(5.0f);
                OpenUpTheScoringMenu();
                StartCoroutine(AmplifyTheMenu(1.5f));

                BoilSound.Stop();
                FleeSound.Play();                               
                
                yield return new WaitForSeconds(1.5f);
                
                switch(currentStage)
                {
                    case GameStage.Fish:
                        StartCoroutine(ScoringEffect(0.0f, eatenFish, 1.0f) );
                        break;
                    case GameStage.Fruits:
                        StartCoroutine(ScoringEffect(0.0f, catchFruits, 1.0f));
                        break;
                    case GameStage.Heating:
                        StartCoroutine(ScoringEffect(0.0f, roastedChicken, 1.0f));
                        break;
                }
                
                yield return new WaitForSeconds(1.0f);
                
                scoringPhaseDone = true;
            }

            private IEnumerator AmplifyTheMenu(float seconds)
            {
                float originScale = 0.5f;
                for(float time = 0.0f; time < seconds; time += Time.deltaTime)
                {
                    WholeMenu.transform.localScale = new Vector3(originScale, originScale, originScale);
                    yield return new WaitForSeconds(Time.deltaTime);
                    originScale = 0.5f * (seconds - time) + 1.0f * (time);                    
                }
            }

            private void PlacingTheFoodCover()
            {
                Food_Cover.transform.position = (foodCoverPosition.position);
            }
    #endregion

    #endregion

    #region 把手震動的反饋

    public IEnumerator Pulse(float pulseLength, bool left)
    {
        for (float i = 0; i < pulseLength; i += Time.deltaTime)
        {
            if (left)
                ViveInput.TriggerHapticPulse(HandRole.LeftHand, (ushort)Mathf.Lerp(0.0f, 4500.0f, 0.7f));
            else
                ViveInput.TriggerHapticPulse(HandRole.RightHand, (ushort)Mathf.Lerp(0.0f, 4500.0f, 0.7f));

            yield return null;
        }
    }

    #endregion

}
