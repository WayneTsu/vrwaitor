﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateManager : MonoBehaviour {

    //  遊戲中盤子部分的互動 (碰撞控制、條件觸發

    #region 區域變數
    [SerializeField]
    private GameObject leftController, rightController; //  把手
    
    private GameObject leftPlate, rightPlate;   //  左手、右手器具
    [SerializeField]
    private GameObject platePrefab;             //  盤子的 prefab
    [SerializeField]
    private GameObject torchGunPrefab;          //  噴槍的 prefab


    #endregion

    void Awake()
    {

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    
}
