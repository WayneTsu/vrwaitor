﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class StageManager : MonoBehaviour {

    [Header("Fish Equipment")]

    [SerializeField]        //  魚的 prefab
    private GameObject fishPrefab;

    [SerializeField]
    private GameObject stick;

    /// <summary>
    /// 器材就定位。(盤子、噴火槍...etc
    /// </summary>
    void toolPrepared_Fish()
    {
        stick.SetActive(true);
        rightHandPlate.SetActive(true);
    }

    /// <summary>
    /// 食物就定位。
    /// </summary>
    void foodPrepared_Fish()
    {
        //  Instantiate(fishPrefab, leftHandFoodPos.position, Quaternion.Euler(0.0f, 90.0f, 90.0f) );
        Instantiate(fishPrefab, rightHandFoodPos.position, Quaternion.Euler(0.0f, 90.0f, 90.0f));
    }

    /// <summary>
    /// 發射物就定位。
    /// </summary>
    void shotManagerPrepared_Fish()
    {
        //ShotManage.instance.ThrowObject = new GameObject[3];

        //ShotManage.instance.ThrowObject[0] = cat01Prefab;
        //ShotManage.instance.ThrowObject[1] = cat02Prefab;
        //ShotManage.instance.ThrowObject[2] = cat03Prefab;
    }

    public void createNewFishPrefab()
    {
        GameObject rightPlate = GameObject.FindGameObjectWithTag("RightPlate");

        Instantiate(fishPrefab, rightPlate.transform.position + new Vector3(0.0f, 1.0f, 0.0f), Quaternion.Euler(0.0f, 90.0f, 90.0f));
    }
}
