﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class StageManager : MonoBehaviour    {

    [SerializeField]
    private GameObject slimePrefab;     //  史萊姆的 prefab

    // <summary>
    /// 器材就定位。
    /// </summary>
    void toolPrepared_Slime()
    {
        leftHandPlate.SetActive(true);
        rightHandPlate.SetActive(true);
    }

    /// <summary>
    /// 食物就定位。
    /// </summary>
    void foodPrepared_Slime()
    {
        Instantiate(slimePrefab, rightHandFoodPos.position, Quaternion.Euler(0.0f, 90.0f, 90.0f));
    }

    /// <summary>
    /// 發射物就定位。
    /// </summary>
    void shotManagerPrepared_Slime()
    {

    }
}
