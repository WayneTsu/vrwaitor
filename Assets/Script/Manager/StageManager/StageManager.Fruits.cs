﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class StageManager : MonoBehaviour {
    
    [Header("Fruits Equipment")]

    [SerializeField]
    private GameObject[] FruitsPrefab;      //  投擲水果的 Prefab   

    [SerializeField]
    private GameObject goalEffect;          //  成功 catch 到水果的特效

    /// <summary>
    /// 器材就定位。
    /// </summary>
    void toolPrepared_Fruits()
    {
        leftHandPlate.SetActive(true);
        rightHandPlate.SetActive(true);
    }

    /// <summary>
    /// 食物就定位。
    /// </summary>
    void foodPrepared_Fruits()
    {
        //  No foods on hands.
    }

    /// <summary>
    /// 發射物就定位。
    /// </summary>
    void shotManagerPrepared_Fruits()
    {        
        ShotManage.instance.ThrowObject = new GameObject[FruitsPrefab.Length];

        for (int i = 0; i < FruitsPrefab.Length; i++)
            ShotManage.instance.ThrowObject[i] = FruitsPrefab[i];
    }

    public void CatchingFruits(Transform _transform)
    {
        GameManager.instance.catchFruits += 1;

        Instantiate(goalEffect, _transform.position, Quaternion.identity);
    }

}
