﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class StageManager : MonoBehaviour {

    //  遊戲內容控制 (掌管遊戲的實際遊玩內容與輸贏條件

    [Header("Shared Variables")]
    #region 區域變數

    [SerializeField]
    private GameObject countDownPrefab; //  倒數計時的 prefab

    [SerializeField]
    private GameObject leftHandPlate;   //  左手盤
    [SerializeField]
    private GameObject rightHandPlate;  //  右手盤

    [SerializeField]
    private Transform leftHandFoodPos, rightHandFoodPos;    //  左右手食物的預設位置
    #endregion

    void Awake()
    {
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    
	}

    /// <summary>
    /// 進入新關卡的倒數計時。(+人聲)
    /// </summary>
    /// <returns></returns>
    private IEnumerator Enter_NewStage()
    {
        countDownPrefab.GetComponent<AudioSource>().Play();
        countDownPrefab.SendMessage("StartCountingDown", 3.0f);

        yield return new WaitForSeconds(0.0f);
    }

    /// <summary>
    /// Rabbit 關卡
    /// </summary>
    private IEnumerator Rabbit_Motion()
    {
        yield return new WaitForSeconds(3.5f);
    }

    /// <summary>
    /// 把所有物件都隱藏。
    /// </summary>
    public void HideEveryTools()
    {
        leftHandPlate.SetActive(false);
        rightHandPlate.SetActive(false);

        battery_prefab.SetActive(false);
        stick.SetActive(false);

        hideHeatingTools();
    }

    public void toolPrepared(GameManager.GameStage currentStage)
    {
        switch(currentStage)
        {
            case GameManager.GameStage.Fish:
                gameObject.SendMessage("toolPrepared_Fish");
                break;
            case GameManager.GameStage.Fruits:
                gameObject.SendMessage("toolPrepared_Fruits");
                break;
            case GameManager.GameStage.Heating:
                gameObject.SendMessage("toolPrepared_Heating");
                break;
            //case GameManager.GameStage.Rabbit:
            //    gameObject.SendMessage("toolPrepared_Rabbit");
            //    break;
            //case GameManager.GameStage.Slime:
            //    gameObject.SendMessage("toolPrepared_Slime");
            //    break;
        }
    }

    public void foodPrepared(GameManager.GameStage currentStage)
    {
        switch (currentStage)
        {
            case GameManager.GameStage.Fish:
                gameObject.SendMessage("foodPrepared_Fish");
                break;
            case GameManager.GameStage.Fruits:
                gameObject.SendMessage("foodPrepared_Fruits");
                break;
            case GameManager.GameStage.Heating:
                gameObject.SendMessage("foodPrepared_Heating");
                break;
        }
    }

    public void shotPrepared(GameManager.GameStage currentStage)
    {
        switch (currentStage)
        {
            case GameManager.GameStage.Fish:
                gameObject.SendMessage("shotManagerPrepared_Fish");
                break;
            case GameManager.GameStage.Fruits:
                gameObject.SendMessage("shotManagerPrepared_Fruits");
                break;
            case GameManager.GameStage.Heating:
                gameObject.SendMessage("shotManagerPrepared_Heating");
                break;
        }
    }
}
