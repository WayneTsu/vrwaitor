﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class StageManager : MonoBehaviour    {

    [Header("Heating Equipment")]

    [SerializeField]
    private GameObject paintGun;        //  吹風機 : 可以吹走氣泡

    [SerializeField]
    private GameObject heatingPlate;    //  可以加熱的盤子

    [SerializeField]
    private ParticleSystem hot_ball;    //  熱球

    [SerializeField]
    private ParticleSystem ice_ball;    //  冰球

    [SerializeField]
    private GameObject battery_prefab;    //  電池
    // <summary>
    /// 器材就定位。
    /// </summary>
    void toolPrepared_Heating()
    {
        paintGun.SetActive(true);
        heatingPlate.SetActive(true);
    }

    /// <summary>
    /// 食物就定位。
    /// </summary>
    void foodPrepared_Heating()
    {
        
    }

    /// <summary>
    /// 發射物就定位。
    /// </summary>
    void shotManagerPrepared_Heating()
    {
        //ShotManage.instance.ThrowObject = new GameObject[1];
        //ShotManage.instance.ThrowObject[0] = battery_prefab;
        battery_prefab.SetActive(true);
        hot_ball.gameObject.SetActive(true);
        ice_ball.gameObject.SetActive(true);
        hot_ball.Play();
        ice_ball.Play();
    }

    void hideHeatingTools()
    {
        GameManager.instance.roastedChicken = heatingPlate.GetComponent<HeatControl>().Heat;
        heatingPlate.SetActive(false);
        paintGun.SetActive(false);
        hot_ball.Stop();
        ice_ball.Stop();
        hot_ball.gameObject.SetActive(false);
        ice_ball.gameObject.SetActive(false);

    }
}
