﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodCover : MonoBehaviour {

    private bool onHit;
    private AudioSource collideOtherObjects;

    [SerializeField]
    private Transform originPosition;

	// Use this for initialization
	void Start () {
        onHit = false;
        collideOtherObjects = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision other)
    {
        if(GetComponent<Rigidbody>().velocity.sqrMagnitude > 1.0f)
        {
            if (onHit == false)
            {
                onHit = true;
                StartCoroutine(collidingCD());

                collideOtherObjects.Play();
            }
        }        
    }

    private IEnumerator collidingCD()
    {
        yield return new WaitForSeconds(3.0f);
        onHit = false;
    }

    public void FoodCoverMotion()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 1.0f, -0.5f) * 10.0f;
    }    

    public void PrepareFoodPosition()
    {
        transform.position = originPosition.position;
    }
}
