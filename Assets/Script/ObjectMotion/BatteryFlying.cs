﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryFlying : MonoBehaviour {

    [SerializeField]
    private GameObject shotgun;

    private Vector3 Initial;

    private Coroutine refresh;
	// Use this for initialization
	void Start () {
        Initial = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if(GameManager.instance.countDown.GetComponent<CountDown>().CountingDownTaskOver())
        {
            if (Vector3.Distance(transform.position, shotgun.transform.position) < 1.0f)//充電
            {
                transform.position = Initial + new Vector3(0.0f, 0.0f, 3.0f);
                shotgun.GetComponent<TorchGun>().ChargePower();

            }
            transform.position = transform.position - new Vector3(0.0f, 0.0f, 0.05f);

            if (transform.position.z < -7.0f)
                transform.position = Initial;
        }            
    }

        
}
