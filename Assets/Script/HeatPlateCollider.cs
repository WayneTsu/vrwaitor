﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(HeatControl))]
public class HeatPlateCollider : MonoBehaviour {

    private HeatControl heat;
    [SerializeField]
    private ParticleSystem smoke;    // 火球擊中特效 + 音效
    [SerializeField]
    private AudioSource bump_audio;

    [SerializeField]
    private ParticleSystem Icesmoke;    // 冰球擊中特效 + 音效

    // Use this for initialization
    void Start () {
        heat = GetComponent<HeatControl>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnParticleCollision(GameObject particle)
    {
        Debug.Log("Hit");
        
        if(ShotManage.gameStart)
        {
            if (particle.gameObject.name == "Fire_ball")
            {
                //Destroy(particle);
                //Instantiate(explosion, transform.position, Quaternion.identity);
                StartCoroutine(smoking());
                heat.Heat += 15.0f;
            }
            else if (particle.gameObject.name == "Ice_ball")
            {
                StartCoroutine(cooling());
                heat.Heat -= 5.0f;
            }
        }        
        
    }

    private IEnumerator smoking()
    {
        smoke.gameObject.transform.position = transform.position;
        smoke.Play();
        bump_audio.Play();
        yield return new WaitForSeconds(1.0f);
        smoke.Stop();
        //bump_audio.Stop();
    }

    private IEnumerator cooling()
    {
        Icesmoke.gameObject.transform.position = transform.position;
        Icesmoke.Play();
        bump_audio.Play();
        yield return new WaitForSeconds(1.0f);
        Icesmoke.Stop();
        //bump_audio.Stop();
    }
}
