﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Balance : MonoBehaviour {

    private float Gravity = 9.8f;
    private Rigidbody rigid;
	// Use this for initialization
	void Start () {
        rigid = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.F))
        {
            rigid.velocity /= 10;
        }
        if (Input.GetKey(KeyCode.M))
        {
            transform.Rotate(transform.forward,1.0f);
        }
        if (Input.GetKey(KeyCode.N))
        {
            transform.Rotate(transform.forward, -1.0f);
        }
	}
}
