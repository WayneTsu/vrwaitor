﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(LineRenderer))]
public class AddLine : MonoBehaviour {

    private LineRenderer line;
    private List<Vector3> vertexList;
    private int Num_of_point_on_circle = 60;
    public float Radius = 0.6f;
    Vector3 local_pos;
    Vector3 local_axis;
    Vector3 local_temp_dir;
    private void SetupVertexPosition(Vector3 pos){
        float angle = 360.0f / Num_of_point_on_circle;
        Vector3 rotatedAxis;
        Vector3 forward;
        Vector3 dir;


        rotatedAxis = Vector3.up;
        forward = Vector3.right;

        for (int i = 0; i < Num_of_point_on_circle; i++)
        {

            //Quaternion.
            var rot = Quaternion.AngleAxis(angle * i, rotatedAxis);
            dir = rot * forward;
            vertexList.Add(pos + dir * Radius);
           
        }
        vertexList.Add(pos + Vector3.right * Radius);
        

        rotatedAxis = Vector3.up;
        forward = Vector3.right;
        
        for (int i = 0; i < Num_of_point_on_circle; i++)
        {
            
            //Quaternion.
            var rot = Quaternion.AngleAxis(angle * i, rotatedAxis);
            dir = rot * forward;
            vertexList.Add(pos + dir * Radius);
            if(dir == Vector3.forward){
                rotatedAxis = Vector3.right;
                forward = Vector3.forward;
                //vertexList.Add(pos + Vector3.forward * Radius);
                for (int j = 0; j < Num_of_point_on_circle; j++)
                //for (int j = 0; j < Num_of_point_on_circle; j++)
                {
                    rot = Quaternion.AngleAxis(angle * j, rotatedAxis);
                    dir = rot * forward;
                    vertexList.Add(pos + dir * Radius);
                    //刻度
                    local_pos = pos + dir * Radius;
                    //local_axis = Vector3.Cross(dir,forward);
                    
                    rot = Quaternion.AngleAxis(angle * 1, forward);
                    local_temp_dir = rot * dir;
                    vertexList.Add(pos + local_temp_dir * Radius);
                    rot = Quaternion.AngleAxis(angle * -1, forward);
                    local_temp_dir = rot * dir;
                    vertexList.Add(pos + local_temp_dir * Radius);


                    vertexList.Add(pos + dir * Radius);
                }

                vertexList.Add(pos + Vector3.forward * Radius);
               
            }
            
            rotatedAxis = Vector3.up;
            forward = Vector3.right;
            
        }
        vertexList.Add(pos + Vector3.right * Radius);
        
        rotatedAxis = Vector3.forward;
        forward = Vector3.right;
        for (int j = 0; j < Num_of_point_on_circle; j++)
        {
            var rot = Quaternion.AngleAxis(angle * j, rotatedAxis);
            dir = rot * forward;
            vertexList.Add(pos + dir * Radius);

            //刻度
            local_pos = pos + dir * Radius;
            //local_axis = Vector3.Cross(dir,forward);

            rot = Quaternion.AngleAxis(angle * 1, forward);
            local_temp_dir = rot * dir;
            vertexList.Add(pos + local_temp_dir * Radius);
            rot = Quaternion.AngleAxis(angle * -1, forward);
            local_temp_dir = rot * dir;
            vertexList.Add(pos + local_temp_dir * Radius);


            vertexList.Add(pos + dir * Radius);
        }   
            //vertexList.Add(pos + forward * Radius);
          
        //vertexList.Add(pos + forward * Radius);

        /*rotatedAxis = Vector3.forward;
        forward = Vector3.right;

        for (int i = 0; i < Num_of_point_on_circle; i++)
        {
            //Quaternion.
            //Quaternion.AngleAxis(角度, 旋轉軸向量)
            //var rot = Quaternion.Euler(angle * i, 0, 0);
            var rot = Quaternion.AngleAxis(angle * i, rotatedAxis);
            dir = rot * forward;
            //vertices[i] = dir * GraphicThickness;
            vertexList.Add(pos + dir * GraphicThickness);

        }  
        vertexList.Add(pos + forward * GraphicThickness);*/
        
    }
	// Use this for initialization
	void Start () {
        vertexList = new List<Vector3>();
        line = GetComponent<LineRenderer>();
        //line = gameObject.AddComponent<LineRenderer>();
        //line.SetVertexCount((Num_of_point_on_circle + 1) * 10 + 3);
        line.SetWidth(0.008f,0.008f);
	}
	
	// Update is called once per frame
	void Update () {
        vertexList = new List<Vector3>();
        SetupVertexPosition(transform.position);
        line.SetPositions(vertexList.ToArray());
        if (Input.GetKey(KeyCode.F))
        {
            //transform.position += transform.right;
        }
        //line.SetPosition(Num_of_point_on_circle,vertexList[0]);
	}
}
