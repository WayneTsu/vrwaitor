﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatLauncher : MonoBehaviour {

    [SerializeField]
    private GameObject[] catPrefabs;
    [SerializeField]
    private Transform[] launchingPoints;

    private GameObject rightPlate;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LaunchingSetActive()
    {
        rightPlate = GameObject.FindGameObjectWithTag("RightPlate");
        StartCoroutine(launching());
    }

    private IEnumerator launching()
    {
        while(ShotManage.gameStart)
        {
            float time = Random.Range(0.5f, 1.0f);
            yield return new WaitForSeconds(time);

            int catIND = Random.Range(0, 3);
            int point = Random.Range(0, 3);
            GameObject cat = Instantiate( catPrefabs[catIND],  launchingPoints[point].position, Quaternion.identity) as GameObject;
            cat.transform.LookAt(rightPlate.transform.position);

            GameObject fish = GameObject.FindGameObjectWithTag("Fish");
            cat.GetComponent<CatMotion>().purring();

            if (fish)
            {
                cat.GetComponent<Rigidbody>().velocity = (rightPlate.transform.position - cat.transform.position + new Vector3(0.0f, 1.5f, 0.0f)) * 1.5f;
            } else
            {
                cat.GetComponent<Rigidbody>().velocity = (rightPlate.transform.position - cat.transform.position + new Vector3(0.0f, 2.5f, 0.0f)) * 1.5f;
            }

        }
    }
}
