﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotManage : MonoBehaviour {

    public static ShotManage instance;

    public static bool gameStart, ready = true;

    public int Num_bullet = 5;
    public GameObject target;
    //public static int ObjectSize;

    //  飛行物
    public GameObject[] ThrowObject;

    [SerializeField]
    private GameObject catLauncher;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
   
    // Use this for initialization
    void Start () {
        dir = target.transform.position - transform.position;
        gameStart = false;

        ThrowObject = new GameObject[6];
    }
    private Vector3 dir;

    // Update is called once per frame
    void Update()
    {
        if (gameStart && ready)
        {
            ready = false;

            //ISSUE 02: 發射的物件太少、太單調
            switch(GameManager.instance.currentStage)
            {
                case GameManager.GameStage.Fish:
                    Num_bullet = 1;
                    break;
                case GameManager.GameStage.Fruits:
                    Num_bullet = 10;
                    break;
                case GameManager.GameStage.Heating:
                    Num_bullet = 1;
                    
                    break;
            }

            for (int i = 0; i < Num_bullet; i++)
            {
                switch(GameManager.instance.currentStage)
                {
                    case GameManager.GameStage.Fish:
                        callingCats();
                        break;
                    case GameManager.GameStage.Heating:
                        
                        break;
                    case GameManager.GameStage.Fruits:
                        createNewShooter();
                        break;
                }                
            }
        }
    }

    //  For Stage Fish.
    public void callingCats()
    {
        catLauncher.GetComponent<CatLauncher>().LaunchingSetActive();
    }

    //  For Stage Fruit.
    public void createNewShooter()
    {
        if (gameStart == false)
            return;

        Vector3 position = new Vector3(0, 0, Random.Range(-2.5f, 2.5f));

        //  ISSUE : 射出的 'FlyingObjects' 種類太少。
        int objectid = Random.Range(0, ThrowObject.Length);

        GameObject go = GameObject.Instantiate(ThrowObject[objectid]) as GameObject;
        go.transform.position = transform.position;
        go.transform.LookAt(position - dir);

        if (objectid == 0 || objectid == 4){
            go.GetComponent<Rigidbody>().velocity = (8 * go.transform.forward +
                          Random.Range(-1.5f, -0.5f) * go.transform.right +
                          11 * Vector3.up) * 1 / 2;
        }
        else if(objectid == 1){
            go.GetComponent<Rigidbody>().velocity = (8 * go.transform.forward+
                          Random.Range(0.5f, 1f) * go.transform.right +
                          11 * Vector3.up) * 1 / 2;
        }
        else {
            go.GetComponent<Rigidbody>().velocity = (8 * go.transform.forward +
                          Random.Range(1.5f, 2.5f) * go.transform.right +
                          11 * Vector3.up) * 1 / 2;
        }
    }
    

}
