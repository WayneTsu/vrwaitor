﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombMotion : MonoBehaviour {

    [SerializeField]
    private GameObject explosionPrefab;

    private bool onHit;
    private int stayTime;
    private Coroutine selfDestruction;

	// Use this for initialization
	void Start () {
        onHit = false;

        selfDestruction = StartCoroutine(CountDown(10.0f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //  ISSUE 04 : 炸彈
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("LeftPlate") || other.gameObject.CompareTag("RightPlate"))
        {
            //  Audio + Effect
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);

            Collider[] colliders = Physics.OverlapSphere( other.transform.position, 1.0f );
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(3.0f, transform.position, 1.0f, 3.0F);
            }

            Destroy(gameObject);
        }
        else if (other.gameObject.CompareTag("Floor") )
        {
            //  Audio + Effect
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);

            Collider[] colliders = Physics.OverlapSphere(other.transform.position, 1.5f);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(3.0f, transform.position, 1.5f, 3.0F);
            }

            Destroy(gameObject);
        }
    }

    private IEnumerator CountDown(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        Destroy(gameObject);
    }
}
