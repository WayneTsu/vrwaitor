﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatMotion : MonoBehaviour {

    [SerializeField]
    private AudioSource catHawl;
    [SerializeField]
    private AudioSource catPurr;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () { 

	}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Fish"))
        {
            if(other.gameObject.GetComponent<FishMotion>().onGoing == true)
            {
                GameManager.instance.eatenFish += 1;
                catHawl.Play();
                StartCoroutine(GameManager.instance.respondToMissingFood(3.0f));

                Destroy(other.gameObject);
            }            
        }
        else if(other.gameObject.CompareTag("Floor"))
        {
            Destroy(gameObject, 0.2f);
        }
    }

    public void purring()
    {
        catPurr.Play();
    }

    public void StartMoving(int path)
    {
        string[] routeName = { "CatPath_01", "CatPath_02", "CatPath_03" };
        float speed = 1.0f;

        iTween.MoveTo(gameObject,
                    iTween.Hash("path", iTweenPath.GetPath(routeName[path]),
                                "speed", speed,
                                "orienttopath", true));
    }
}
