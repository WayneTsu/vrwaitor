﻿using UnityEngine;
using System.Collections;
using System;
[RequireComponent(typeof(Rigidbody))]
public class ShotBehavior : MonoBehaviour {

    /// <summary>
    /// 第一次打中地面? 是 : 產生一個新的 飛行物件 ; 否 : 不作為。
    /// </summary>
    private bool onHit;

    private int stayCollision = 0;

    private Coroutine selfDestruction;

    // Use this for initialization
    void Start() {
        onHit = false;
        stayCollision = 0;

        selfDestruction = StartCoroutine(countDown());
    }

    // Update is called once per frame
    void Update() {
        if (ShotManage.gameStart == false)
            Destroy(gameObject);
    }
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Floor")
        {
            if(!onHit)
            {
                onHit = true;
                ShotManage.instance.createNewShooter();
            }
            
            Destroy(gameObject, 1.5f);
        }
        else if(other.gameObject.tag == "LeftPlate")
        {
            stayCollision = 0;
            StopCoroutine(selfDestruction);

            onHit = false;                      
        }
        else if(other.gameObject.tag == "RightPlate")
        {
            stayCollision = 0;            
            StopCoroutine(selfDestruction);

            onHit = false;
        }
    }

    void OnCollisionStay(Collision other)
    {
        if(other.gameObject.tag == "LeftPlate" || other.gameObject.tag == "RightPlate")
        {
            stayCollision += 1;

            if( stayCollision > 150 )
            {
                GameManager.instance.CatchingFruits(transform);

                if(!onHit)
                {
                    onHit = true;
                    ShotManage.instance.createNewShooter();
                }

                Destroy(gameObject);
            }
        }
    }

    void OnCollisionExit(Collision other)
    {
        if(other.gameObject.tag == "LeftPlate" || other.gameObject.tag == "RightPlate")
        {
            selfDestruction = StartCoroutine(countDown());
            onHit = false;
        }
    }

    private IEnumerator countDown()
    {
        yield return new WaitForSeconds(10.0f);

        if (!onHit)
        {
            onHit = true;
            ShotManage.instance.createNewShooter();
        }

        Destroy(gameObject);
    }
}