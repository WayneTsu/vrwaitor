﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    #region variables

    [SerializeField]
    private Text timerText;
    [SerializeField]
    private Image timerValue;

    private AudioSource countDownVoice;
    private string[] countDownText;
    #endregion

    // Use this for initialization
    void Start () {
        countDownVoice = gameObject.GetComponent<AudioSource>();

        countDownText = new string[4];
        countDownText[0] = "THREE";
        countDownText[1] = "TWO";
        countDownText[2] = "ONE";
        countDownText[3] = "GO!";

        StartCoroutine( TimerStart() );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator TimerStart()
    {
        float previous = -1.0f;
        countDownVoice.Play();
        for(float i = 0.0f; i < 3.0f; i+= Time.deltaTime * 2 )
        {
            yield return new WaitForSeconds(Time.deltaTime);
            float ceiling = Mathf.Ceil(i);
            timerValue.fillAmount =  (ceiling - i) / 1.0f;
            float floor = Mathf.Floor(i);

            if(previous != floor)
            {
                if (floor == 0.0f)
                    timerText.text = countDownText[0];
                else if(floor == 1.0f)
                    timerText.text = countDownText[1];
                else if (floor == 2.0f)
                    timerText.text = countDownText[2];

                previous = floor;
            }
        }

        timerText.text = countDownText[3];

        yield return new WaitForSeconds(0.5f);

        Destroy(gameObject);
    }
}
