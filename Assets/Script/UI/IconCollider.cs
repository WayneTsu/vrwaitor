﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class IconCollider : MonoBehaviour {

    public bool isReady = false;
    [HideInInspector]
    public bool activated;
    
    [SerializeField]
    private Transform leftController, rightController;  //  左右手的把手
                                                        //  Use this for initialization
    [SerializeField]
    private GameManager.GameStage representation;       //  代表的關卡種類

    public AudioSource Audio_hovering, Audio_Trigger;
    private Animator Anim;
    private bool IsFirstHover = true;
    void Start () {
        Anim = GetComponent<Animator>();
        activated = true;
	}
	
	// Update is called once per frame
	void Update () {

        if ( (Vector3.Magnitude(leftController.position - transform.position) < 0.3f || Vector3.Magnitude(rightController.position - transform.position) < 0.3f) 
            && activated)
        {
            if(!Audio_hovering.isPlaying && IsFirstHover)
            {
                Audio_hovering.PlayDelayed(0.1f);
                IsFirstHover = false;
            }
                
            Anim.SetBool("IsTouching",true);
            if ((ViveInput.GetPress(HandRole.LeftHand, ControllerButton.Trigger) ||
                               ViveInput.GetPress(HandRole.RightHand, ControllerButton.Trigger)))
            {
                if (!Audio_Trigger.isPlaying)
                    Audio_Trigger.Play();
                Anim.SetBool("IsTrigger",true);

                GameManager.instance.HeadingToNewStage(representation);
                isReady = true;
            }
        }
        else
        {
            IsFirstHover = true;
            Anim.SetBool("IsTouching", false);
        }
           
    }

    public void resetButtonStatus()
    {
        isReady = false;
        IsFirstHover = true;
        activated = false;

        Anim.SetBool("IsTouching", false);
        Anim.SetBool("IsTrigger", false);
    }

}
