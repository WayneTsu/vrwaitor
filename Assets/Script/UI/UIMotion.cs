﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMotion : MonoBehaviour
{
    //  針對UI字型的特殊效果。

    #region 變數
    private Text UIText;
    
    private bool blinking = false;  //  閃爍效果
    private bool floating = false;  //  懸浮效果
    
    #endregion

    // Use this for initialization
	void Start () {
        UIText = gameObject.GetComponent<Text>();

        SetFloating(true);
        SetBlink(true);
	}
	
	// Update is called once per frame
	void Update () {
	}

    #region Blink Motion

    public void SetBlink(bool status)
    {
        blinking = status;

        if (blinking)
            StartCoroutine(Text_Blink());
    }

    private IEnumerator Text_Blink()
    {
        float offset = -0.1f;
        while (blinking)
        {
            Color current = new Color(UIText.color.r, UIText.color.g, UIText.color.b, UIText.color.a + offset);
            UIText.color = current;

            yield return new WaitForSeconds(0.1f);

            if (current.a >= 1.0f || current.a <= 0.4f)
                offset = -offset;
        }
    }

    #endregion

    #region Floating Motion

    public void SetFloating(bool status)
    {
        floating = status;

        if (floating)
            StartCoroutine(Text_Floating());
    }

    private IEnumerator Text_Floating()
    {
        Vector3 directionOffset = Vector3.up.normalized / 100.0f;
        Vector3 currentFloatingOffset = Vector3.zero;
        float distance = 0.08f;

        while (floating)
        {
            UIText.gameObject.transform.Translate(directionOffset);
            currentFloatingOffset += directionOffset;

            yield return new WaitForSeconds(Time.deltaTime * 2.0f);

            if (currentFloatingOffset.magnitude > distance)
                directionOffset = -directionOffset;
        }
    }

    #endregion
}
