﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveInputGetReady : MonoBehaviour {

    //  測試 viveinput 是否已經就位 : 盤面向上 + 位置靠近

    [SerializeField]
    private Transform leftHand, rightHand;  //  左右手的預期位置
    [SerializeField]
    private Transform leftController, rightController; //  左右手的把手

    [SerializeField]
    private GameObject leftArrow, rightArrow;

    private bool leftReady, rightReady;

	// Use this for initialization
	void Start () {
        leftReady = false;
        rightReady = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Magnitude(leftController.position - leftHand.position) < 0.3f)
        {
            if(leftReady == false)
            {
                leftReady = true;
                leftArrow.GetComponent<AudioSource>().Play();
            }

            leftArrow.GetComponent<Arrow>().SetArrowColor(Color.green);
        }            

        if(Vector3.Magnitude(rightController.position - rightHand.position) < 0.3f)
        {
            if(rightReady == false)
            {
                rightReady = true;
                rightArrow.GetComponent<AudioSource>().Play();
            }

            rightArrow.GetComponent<Arrow>().SetArrowColor(Color.green);
        }
    }

    /// <summary>
    /// 左右手的把手靠近目的位置, 並且方向朝上
    /// </summary>
    public bool IsReady()
    {
        bool leftVivePositionReady = Vector3.Magnitude( leftController.position - leftHand.position ) < 0.3f;
        bool rightVivePositionReady = Vector3.Magnitude( rightController.position - rightHand.position ) < 0.3f;

        bool leftViveRotationReady = Vector3.Dot( leftController.up, leftHand.up ) > 0.0f;
        bool rightViveRotationReady = Vector3.Dot(rightController.up, rightHand.up) > 0.0f;

        //  符合所有條件
        return (leftVivePositionReady && rightVivePositionReady &&
                leftViveRotationReady && rightViveRotationReady);
    }
}
