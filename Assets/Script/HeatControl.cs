﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(MeshRenderer)),RequireComponent(typeof(AudioSource))]
public class HeatControl : MonoBehaviour {

    public float Heat;
    public ParticleSystem particle;
    private Material mat;
    public AudioSource hot_audio;
    public AudioSource break_audio;
    private float time = 0.0f;
    public GameObject Fragment;
    [SerializeField]
    public int Max = 100;
    [SerializeField]
    public int Min = 0;

    private bool breaking = false;
	// Use this for initialization
	void Start () {
        mat = GetComponent<MeshRenderer>().material;
        GetComponent<MeshRenderer>().sharedMaterial = mat;
        mat = GetComponent<MeshRenderer>().material;
        Heat = (Max + Min)/2;
	}
    
	// Update is called once per frame
	void Update () {
        if (ShotManage.gameStart && 
            GameManager.instance.countDownIsOver )
        {
            updatematerial();
            if (Heat > Min)
            {
                Heat -= 0.5f * Time.deltaTime;
            }
            if (breaking == true && Time.time - time > 5.0f)
            {
                time = 0.0f;
                if (!break_audio.isPlaying)
                {

                    break_audio.pitch = 0.995f;
                    break_audio.Play();
                }

                Fragment.transform.position = transform.position;
                Fragment.transform.rotation = transform.rotation;
                Fragment.transform.localScale = transform.localScale;

                Fragment.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Amount", mat.GetFloat("_Amount"));
                Debug.Log(mat.GetFloat("_Amount"));
                Fragment.SetActive(true);
                //gameObject.SetActive(false);


            }
        }

        
	}
    void updatematerial()
    {
        if (Heat < Max * 0.3f)
        {
            breaking = false;
            mat.SetFloat("_Amount", 0.0f);
        }
        else
        {
            
            if (Heat > Max * 0.99f)
            {
                mat.SetFloat("_Amount", 1.0f);
                if (breaking == false)
                {
                    time = Time.time;
                    breaking = true;
                }
                
                if (!hot_audio.isPlaying)
                {

                    hot_audio.pitch = 0.995f;
                    hot_audio.Play();
                   

                }
                
            }
            else if (Heat > Max * 0.7f)
            {
                mat.SetFloat("_Amount", (float)(0.0042 * Heat) + 0.57f);
                time = 0.0f;
                breaking = false;
                hot_audio.Stop();
                particle.Play();
            }
            else
            {
                mat.SetFloat("_Amount", (float)(0.0042 * Heat) + 0.57f);
                time = 0.0f;
                breaking = false;
                hot_audio.Stop();
                particle.Stop();
            }
        }
    }

    
}
