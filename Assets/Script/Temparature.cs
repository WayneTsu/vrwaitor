﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Temparature : MonoBehaviour {
    public GameObject BackGround;
    public GameObject Handle;
    public HeatControl heatcontrol;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        BackGround.GetComponent<Image>().fillAmount = (float)heatcontrol.Heat/heatcontrol.Max;
        if (heatcontrol.Heat > heatcontrol.Max)
        {
            if (Mathf.Floor(Time.time*5) % 2 == 0)
            {
                BackGround.SetActive(true);
                Handle.SetActive(true);
            }
            else if (Mathf.Floor(Time.time*5) % 2 == 1)
            {
                BackGround.SetActive(false);
                Handle.SetActive(false);
            }
        }
        else
        {
            BackGround.SetActive(true);
            Handle.SetActive(true);
        }
        
	}
}
