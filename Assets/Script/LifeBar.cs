﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(LineRenderer))]
public class LifeBar : MonoBehaviour {
    public Camera cam;
    private int Num_of_point_on_circle = 60;
    private LineRenderer line;
    private List<Vector3> vertexList;
    public float Radius = 1.0f;
    public int Life;
	// Use this for initialization
	
    private void SetupLife(Vector3 pos, int life)
    {
        float angle = 360.0f / Num_of_point_on_circle;
        Vector3 rotatedAxis;
        Vector3 forward;
        Vector3 dir = Vector3.zero;
		Quaternion rot;
        rotatedAxis = cam.transform.position - transform.position;
        forward = Vector3.up;
        for (int i = 0; i < Num_of_point_on_circle+1; i++)
        {
			if(i <= life){
				//Quaternion.
				rot = Quaternion.AngleAxis(angle * i, rotatedAxis);
				dir = rot * forward;
				vertexList.Add(pos + dir * Radius);
			}
			else{
				vertexList.Add(pos + dir * Radius);
			}
            
        }
        //if(life == 60)
            //vertexList.Add(pos + forward * Radius);
    }
	void Start () {
        vertexList = new List<Vector3>();
        line = GetComponent<LineRenderer>();
        //line.SetVertexCount((Num_of_point_on_circle + 1));
        //line.SetWidth(0.5f, 0.5f);
	}
	// Update is called once per frame
	void Update () {
        vertexList = new List<Vector3>();
        //line.SetVertexCount(Num_of_point_on_circle);
        SetupLife(transform.position,Life);
        line.SetPositions(vertexList.ToArray());
	}
}
