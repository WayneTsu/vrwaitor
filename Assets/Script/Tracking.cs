﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;

public class Tracking : MonoBehaviour {

    public GameObject controller;   //  Controller.
    private Rigidbody rigid;        //  Rigidbody of the Plate.

    [SerializeField]
    private GameObject explosion;    // 擊中特效 + 音效
    

    // Use this for initialization
	void Start () {
        rigid = GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        #region ver.01 : UnFunction
        //rigid.MovePosition(controller.transform.position + localPosition);
        //Quaternion newRotation = controller.transform.rotation * Quaternion.Euler(localRotation.x, localRotation.y, localRotation.z);
        //rigid.MoveRotation( newRotation );
        #endregion

        #region ver.02 : UnFunction
        //Vector3 handPos = controller.transform.position + localPosition;
        //Quaternion handRot = controller.transform.rotation * Quaternion.Euler(localRotation.x, localRotation.y, localRotation.z);

        //rigid.MovePosition(Vector3.Lerp(rigid.transform.position, handPos, Time.deltaTime * 0.3f) );
        //rigid.MoveRotation(Quaternion.Slerp(rigid.transform.rotation, handRot, Time.deltaTime * 0.3f));
        #endregion

        #region ver.03 : Function
        //  Position Realtime Update.
        Vector3 positionDelta =  ( controller.transform.position ) - gameObject.transform.position;
        rigid.velocity = positionDelta * 900.0f * Time.deltaTime;
        //  Rotation Realtime Update.
        //rigid.MoveRotation(Quaternion.Slerp(rigid.transform.rotation, controller.transform.rotation, 1.0f));

        if(gameObject.tag == "Stick")
        {
            Quaternion stickFacing = Quaternion.Euler( controller.transform.right * 90.0f );

            //rigid.MoveRotation( Quaternion.FromToRotation( rigid.transform.rotation.eulerAngles, (stickFacing * controller.transform.rotation).eulerAngles ));
            rigid.MoveRotation( (stickFacing * controller.transform.rotation) );
        } else
        {
            rigid.MoveRotation(Quaternion.Slerp(rigid.transform.rotation, controller.transform.rotation, 1.0f));
        }               
       
        #endregion
    }

    //  盤子受到碰撞。
    void OnCollisionEnter(Collision other)
    {
        //  受到 tag 為 "FlyingObjects" 的碰撞才要做反應。
        if(other.gameObject.CompareTag("FlyingObjects"))
        {
            if (gameObject.tag == "LeftPlate")
                StartCoroutine(GameManager.instance.Pulse(0.5f, false));

            else if(gameObject.tag == "RightPlate" || gameObject.tag == "Stick")
                StartCoroutine(GameManager.instance.Pulse(0.5f, true));
            //  加分。
            //  加分 UI。

            //  音效，
            //  特效。
            Instantiate(explosion, other.transform.position, Quaternion.identity);
        }
               
    }
}
